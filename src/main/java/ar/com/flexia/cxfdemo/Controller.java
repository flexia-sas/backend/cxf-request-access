package ar.com.flexia.cxfdemo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

@Path("/1")
public class Controller {
	private Service service;

	@Context
	HttpHeaders hh;

	@GET
	@Produces("text/plain")
	public String test() {
		String correlation = hh.getHeaderString("x-correlation-id");
		String correlationFromThread = service.correlationId();
		if (!correlation.equals(correlationFromThread)) {
			System.out.println("ERROR: " + correlation + " : " + correlationFromThread);
		}
		return correlationFromThread;
	}

	public void setService(Service service) {
		this.service = service;
	}
}
