package ar.com.flexia.cxfdemo;

import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;

public class Service {

	public String correlationId() {
		Object requestRef = RequestContextHolder.currentRequestAttributes().resolveReference("request");
		assert null != requestRef;
		HttpServletRequest request = (HttpServletRequest) requestRef;
		return request.getHeader("x-correlation-id");
	}
}
