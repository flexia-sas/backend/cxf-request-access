# CXF Request Access

Ejemplo que demuestra cómo acceder a elementos del Request HTTP desde un Bean de Spring con JAXRS CXF como web framework.

Se incluye un archivo _test-plan.jmx_ de JMeter para poder validar que la lectura de headers HTTP sea consistente en un 
escenario de múltiples requests concurrentes.

# Config

La configuración importante consiste en registrar el listener 
_org.springframework.web.context.request.RequestContextListener_ en el archivo web.xml para poder conectar la request 
HTTP de JAXRS al contexto de Spring.

```xml
<listener>
    <listener-class>
        org.springframework.web.context.request.RequestContextListener
    </listener-class>
</listener>
```

Se puede ver el acceso a uno de los headers HTTP contenidos en la request en la clase _ar.com.flexia.cxfdemo.Service_:
```java
public String correlationId() {
    Object requestRef = RequestContextHolder.currentRequestAttributes().resolveReference("request");
    assert null != requestRef;
    HttpServletRequest request = (HttpServletRequest) requestRef;
    return request.getHeader("x-correlation-id");
}
```
_Fuente: https://cxf.apache.org/docs/jaxrs-services-configuration.html#JAXRSServicesConfiguration-Lifecyclemanagement_